try:
    import configparser
except ImportError:
    import ConfigParser as configparser

import LV
import HV
import Chiller
import Interlock
from Sensors import *

import DCSTools as fm

def DCSConfig():
    parser = configparser.ConfigParser()
    parser.read("DCSConfig.ini")
    HV_type=parser['HighVoltage']['Type']
    HV_location=parser['HighVoltage']['Location']
    
    LV_type=parser['LowVoltage']['Type']
    LV_location=parser['LowVoltage']['Location']
        
    Chiller_type=parser['Chiller']['Type']
    Chiller_location=parser['Chiller']['Location']

    interlock_location=parser['Interlock']['location']
    interlock_baud=parser['Interlock']['baud']
    Chill=None
    HighV=None
    LowV=None
    '''
    try:
        LowV=fm.init_device('LV')
        HighV=fm.init_device('HV')
        
    except:
        pass
    '''
    if(not HighV):
        HighV=fm.init_class("HV",HV_type,HV_location)
    if(not LowV):
        LowV=fm.init_class("LV",LV_type,LV_location)
    interlock=Interlock.Cambridge(interlock_location,interlock_baud)

    #Chill=fm.init_class("Chiller",Chiller_type,Chiller_location)
    
    return (HighV,LowV,Chill,interlock,str(parser['MasterServer']['location']),str(parser['MasterServer']['grafana']))    


def SensorConfig():
    parser = configparser.ConfigParser()
    parser.read("SensorConfig.ini")
    sensor_package=fm.package_contents("Sensors")
    sections=parser.sections()
    sensors={}
    for section in sections:
        for sensor_name in sensor_package:
            if sensor_name in section:
                args=parser._sections[section]
                print(section)
                sensor_type=fm.init_class_with_dict("Sensors",sensor_name,args)
                sensors[section]=sensor_type
           
    return sensors

HighVoltage,LowVoltage,interlock,Chill,master,grafana=DCSConfig()
SensorDict=SensorConfig()
#print(SensorDict)
