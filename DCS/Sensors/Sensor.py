
class Sensor:
    def __init__(self,name="Sensor"):
        self.name=name
        self.data={}
        
    def get_data(self):
        return self.data

    def set_name(self,name):
        self.name=name
