import subprocess
import re
# python3 workoaround for SHT sensors
class Sht:
    def __init__(self,clock,data,symbolic_link="sensor.py"):
        self.clock=clock
        self.data=data
        self.symbolic_link=symbolic_link

    def read_t(self):
        commmand=self.symbolic_link+" -v -t "
        command+=str(self.data)+" "+str(self.clock)
        output = subprocess.check_output(command, shell=True)
        result=re.findall(r"-*\d+\.+\d*",output)
        return float(result)

    def read_rh(self,t=None):
        commmand=self.symbolic_link+" -v -r "
        command+=str(self.data)+" "+str(self.clock)
        output = subprocess.check_output(command, shell=True)
        result=re.findall(r"-*\d+\.+\d*",output)
        return float(result)
