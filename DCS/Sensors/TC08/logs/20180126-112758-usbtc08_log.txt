Pico Technology TC-08 thermocouple data logger
Driver version: 1.10.0.9
Kernel driver version: 1.0
Hardware version: 1
Variant: 3
Serial: A0043/462
Calibration date: 29Nov17
Max sample interval: 900 ms
Used sample interval: 900 ms
