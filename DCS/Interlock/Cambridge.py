import serial
import time

#cambridge interlock unit
#untested for now

class Cambridge:
    def __init__(self,location="/dev/ttyACM0",baudrate=115200,outtime=2):
        try:
            self.interlock=serial.Serial("/dev/ttyACM0",baudrate,timeout=outtime)
            ser.write("ena,100\r")
        except serial.serialutil.SerialException:
            print("can't open interlock on "+str(location))
        self.commands={"htl":"hybdrid temp lower limit",
                       "hth":"hybdrid temp upper limit",
                       "hne":"Number of Hybdrid sensors",
                       "hna":"Number of Samples for average",
                       "hrn":"Nominal hybdrid thermistor resistance ",
                       "htn":"Nominal thermistor temperature",
                       "hbn":"Hybdrid Thermistor B parameter",
                       "utl":"User temp lower limit",
                       "uth":"User temp upper limit",
                       "une":"User Number of Hybdrid sensors",
                       "una":"User Number of Samples for average",
                       "urn":"User Nominal hybdrid thermistor resistance ",
                       "utn":"User Nominal thermistor temperature",
                       "ubn":"User Thermistor B parameter",
                       "rtl":"SHT temp lower limit",
                       "rth":"SHT temp lower limit",
                       "rrl":"SHT Humidity lower limit",
                       "rrh":"SHT Humidity Upper limimt",
                       "rne":"Number of SHT sensors"}
        
        
    def get_config(self):
        self.interlock.write("irs, 100\r".encode())
        data=self.interlock.readline()
        config=data[:3]
        try:
            data.replace(config,self.commands[config])
        except KeyError:
            pass
        print(data)
        print(config)
        while data:
            data=self.interlock.readline().decode()
            config=data[:3]
            try:
                data=data.replace(config,self.commands[config])
            except KeyError:
                pass
            print(data[:-2])
        
            
    def set_humidity_range(self,upper_limit=30,lower_limit=0):
        query="rrl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="rrh, "+str(uppper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())


    def set_hybdrid_NTC_temp(self,upper_limit=30,lower_limit=-25):
        query="htl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="hth, "+str(uppper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        
    def set_hybdrid_user_temp(self,upper_limit=30,lower_limit=-25):
        query="utl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="uth, "+str(uppper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        

    def set_SHT_temp(self,upper_limit=30,lower_limit=-25):
        query="rtl, "+str(lower_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())
        query="rth, "+str(uppper_limit)+"\r"
        self.interlock.write(query.encode())
        print(self.interlock.readline())

    def set_B_parameters(self,user=3000,hybdrid=3000):
        query="hbn, "+str(hybdrod)+"\r"
        self.interlock.write(query.encode())
        selt.interlock.readline()
        query="ubn, "+str(hybdrod)+"\r"
        self.interlock.write(query.encode())
        selt.interlock.readline()
        
    def disable(self):
        self.interlock.write("dis,100\r")
        
    def get_data(self):
        self.interlock.write("idr, 100 \r")
        data=self.readline()
        return data
    
if __name__=="__main__":
    #for testing
    cambridge=Interlock()
    cambridge.get_config()

