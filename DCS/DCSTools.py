import imp
import os
import LV
import Interlock
import HV
import serial.tools.list_ports
import Sensors
from SerialDevice import SerialDevice


        
def package_contents(package_name):
    MODULE_EXTENSIONS = ('.py', '.pyc', '.pyo')
    
    file, pathname, description = imp.find_module(package_name)
    if file:
        raise ImportError('Not a package: %r', package_name)
    # Use a set because some may be both source and compiled.
    return set([os.path.splitext(module)[0]
                for module in os.listdir(pathname)
                if (module.endswith(MODULE_EXTENSIONS) and not "__init__" in module.lower() and not "tool" in module.lower() and not module.lower()=="Sensor" ) ])


def init_class(package,obj_name,*args):
    module = __import__(package)
    DeviceType = getattr(module,obj_name )
    device=DeviceType(*args)
    return device

def init_class_with_dict(package,obj_name,dictionary):
    module = __import__(package)
    print(obj_name)
    DeviceType = getattr(module,obj_name )
    device=DeviceType(**dictionary)
    return device

def list_devices():
    devices =serial.tools.list_ports.comports(True)
    deviceList = {}
    if not devices:
        return None
    for i in devices:
        dev=str(i)
        path=dev.split()[0]
        device=SerialDevice(location=path)
        name=device.GetType()
        deviceList[path]=name
    return deviceList

def init_device(device_type):
    '''device type = HV, LV aybe chiller if it can *idn? '''
    list_of_types=package_contents(device_type)
    deviceList=list_devices()
    try:
        print(device)
        for path,name in deviceList.items():
            for device in list_of_types:
                if device.lower() in name.lower():
                    print("initlized "+str(device)+" in "+str(path))
                    return init_class(device_type,device,path)
    except:
        return None
	        
