import serial
import re
import sys, os

class Sorensen:
    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=5):
        try:
            self.soren=serial.Serial(location, baudrate, timeout=outtime)
        except serial.SerialException:
            print("can't open the Sorensen, is it connected on "+location+" ?")

    def __del__(self):
        try:
            self.soren.close()
        except AttributeError:
            pass
    def GetType(self):
        query="*IDN?\n"
        try:
            self.soren.write(query.encode())
            idn=self.soren.readline()
            print(idn)
            return idn
        except serial.SerialTimeoutException:
            print("soren query from GetType() failed")

    def TurnOn(self,channel):
        query="OP"+str(channel)+" 1\n"
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query turn on failed")
            return False

    def TurnOff(self,channel):
        query="OP"+str(channel)+" 0\n"
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query turn off failed")
            return False
        
    def SetVoltage(self,voltage,channel):
        query="V"+str(channel)+" "+str(voltage)+"\n"
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query from Set voltage failed")
            return False
        
    def SetCurrent(self,current,channel):
        query="I"+str(channel)+" "+str(current)+"\n"
        
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query from Set current failed")
            return False
            
    def SetIProtection(self,current,channel):
        query="OCP"+str(channel)+" "+str(current)+"\n"
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query from current protection failed")
            return False
        
    def SetVProtection(self,voltage,channel):
        query="OVP"+str(channel)+" "+str(voltage)+"\n"
        try:
            self.soren.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("soren query from voltage protection failed")
            return False
        
    def ReadVoltage(self,channel):
        query="V"+str(channel)+"O?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read voltage "+str(reply))
            voltage= re.findall(r"-*\d+\.+\d*", str(reply))[0]
            return float(voltage) 
        except serial.SerialTimeoutException:
            print("soren query from read voltage failed")
            return None
        
    def ReadCurrent(self,channel):
        query="I"+str(channel)+"O?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read current "+str(reply))
            current= re.findall(r"-*\d+\.+\d*", str(reply))[0]
            return float(current) 
        except serial.SerialTimeoutException:
            print("soren query from read current failed")
            return None
        
    def ReadSetVoltage(self,channel):
        query="V"+str(channel)+"?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read set voltage "+str(reply))
            voltage= re.findall(r"-*\d+\.+\d*",str(reply))[0]
            return float(voltage) 
        except serial.SerialTimeoutException:
            print("soren query from read set voltage failed")
            return None
        
    def ReadSetCurrent(self,channel):
        query="I"+str(channel)+"?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read set current "+str(reply))
            current= re.findall(r"-*\d+\.+\d*",str(reply))[0]
            return float(current) 
        except serial.SerialTimeoutException:
            print("soren query from read set current failed")
            return None
            
    def ToLocal(self):
        query="Local\n"
        try:
            self.soren.write(query.encode())
            print(self.soren.readline())
        except serial.SerialTimeoutException:
            print("soren query from to local failed")
            
    def Lock(self):
        query="IFLOCK\n"
        try:
            self.soren.write(query.encode())
            print(self.soren.readline())
        except serial.SerialTimeoutException:
            print("soren query from lock failed")
            
    def Unlock(self):
        query="IFUNLOCK\n"
        try:
            self.soren.write(query.encode())
            print(self.soren.readline())
        except serial.SerialTimeoutException:
            print("soren query from unlock failed")

    def IsLocked(self):
        query="IFLOCK?\n"
        try:
            self.soren.write(query.encode())
            print(self.soren.readline())
        except serial.SerialTimeoutException:
            print("soren query from unlock failed")
       
    def GetIProtection(self,channel):
        query="OCP"+str(channel)+"?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read current protection "+str(reply))
            current= re.findall(r"-*\d+\.+\d*",str(reply))[0]
            return float(current) 
        except serial.SerialTimeoutException:
            print("soren query from read current protection failed")
            return None

        
    def GetVProtection(self,channel):
        query="OVP"+str(channel)+"?\n"
        try:
            self.soren.write(query.encode())
            reply=self.soren.readline()
            if(reply==b''):
                raise serial.SerialTimeoutException
            print("Read voltage protection "+str(reply))
            current= re.findall(r"-*\d+\.+\d*",str(reply))[0]
            return float(current) 
        except serial.SerialTimeoutException:
            print("soren query from read voltage protection failed")
            return None
    
# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

if __name__=="__main__":
    brick=Sorensen(location='/dev/ttyACM0')
    brick.GetType()
    brick.Lock()
    brick.SetVoltage(1.1,1)
    brick.ReadSetVoltage(1)
    brick.SetVoltage(1.2,2)
    Iset=brick.ReadSetCurrent(1)
    I=brick.ReadCurrent(1)
    Vset=brick.ReadSetVoltage(1)
    V=brick.ReadVoltage(1)
    print(Iset,I,Vset,V)
    brick.Unlock()
