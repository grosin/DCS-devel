##instek is null modem (not pin to pin, need a specialized cable for serial)

import serial
import time

class Instek:

    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=5):
        try:
            self.device=serial.Serial(location, baudrate, timeout=outtime)
        except serial.SerialException:
            print("can't open the instek, is it connected on "+location+" ?")

    def __del__(self):
        try:
            self.device.close()
        except AttributeError:
            pass
        
    def GetType(self):
        query="*IDN?\n"
        try:
            self.device.write(query.encode())
            idn=self.device.readline()
            print(idn)
            return idn
        except serial.SerialTimeoutException:
            print("instek query from GetType() failed")

    def TurnOn(self):
        query="OUTP:STAT 1\n"
        self.device.write(query.encode())

    def TurnOff(self):
        query="OUTP:STAT 0\n"
        self.device.write(query.encode())
        self.device.flush()

    def SetCurrent(self,Channel=1,value=0):
        query=":CHAN"+str(Channel)+":CURR "+str(value)+"\n"
        print(query)
        self.device.write(query.encode())
        return self.device.readline()

    def SetVoltage(self,Channel=1,value=0):
        query=":CHAN"+str(Channel)+":VOLT "+str(value)+"\n"
        print(query)
        self.device.write(query.encode())
        print(self.device.readline())

    def GetCurrent(self,Channel=1):
        query=":CHAN"+str(Channel)+":CURR ?\n"
        self.device.write(query.encode())
        print(self.device.readline())

    def GetVoltage(self,Channel=1):
        query=":CHAN"+str(Channel)+":VOLT ?\n"
        self.device.write(query.encode())
        print(self.device.readline())

    def GetActualCurrent(self,Channel=1):
        query=":CHAN"+str(Channel)+":MEAS:CURR ? \n"
        self.device.write(query.encode())
        print(self.device.readline())

    def GetActualVoltage(self,Channel=1):
        query=":CHAN"+str(Channel)+":MEAS:VOLT ? \n"
        self.device.write(query.encode())
        print(self.device.readline())

    def TurnOnCurrentProtection(self,Channel=1):
        query=":CHAN"+str(Channel)+":PROT:CURR 1 \n"
        self.device.write(query.encode())

    def TurnOffCurrentProtection(self,Channel=1):
        query=":CHAN"+str(Channel)+":PROT:CURR 0 \n"
        self.device.write(query.encode())

    def IsCurrentProtectionOn(self,Channel=1):
        query=":CHAN"+str(Channel)+":PROTection:CURRent ? \n"
        self.device.write(query.encode())
        print(self.device.readline())

    def SetVoltageProtection(self,Channel=1,value=0):
        query=":CHAN"+str(Channel)+":PROT:VOLT " +str(value)+"\n"
        self.device.write(query.encode())

    def GetVoltageProtection(self,Channel=1):
        query=":CHAN"+str(Channel)+":PROT:VOLT ?\n" 
        self.device.write(query.encode())
        return self.device.readline()

if __name__=="__main__":
    instk=Instek()
    time.sleep(2)
    instk.GetType()
    time.sleep(2)
   
    instk.SetVoltage(Channel=1,value=1.5)
    instk.SetVoltageProtection(1,1.5)
    instk.GetVoltageProtection(1)

