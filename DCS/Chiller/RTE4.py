#class for water chiller
import serial
import time
from Chiller.ChillerTools import SetPoint
from Chiller.ChillerTools import GetPoint
import sys

class RTE4:
    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=3):
        try:
            self.chiller=serial.Serial(location,baudrate,timeout=outtime)
        except serial.SerialException:
            print("can't open the chiller, is it connected on "+location+" ?")
		
    def GetType(self):
            return "Water Chiller"

    def SetPoint1(self,temp):
        try:
            query=SetPoint(temp,1)
            self.chiller.write(query.encode())
            #print(self.chiller.readline())
        except serial.SerialTimeoutException:
            print("Timeout: chiller not responding")

    def SetPoint2(self,temp):
        try:
            query=SetPoint(temp,2)
            self.chiller.write(query.encode())
            #print(self.chiller.readline())
        except serial.SerialTimeoutException:
            print("Timeout: chiller not responding")

    def SetTemperature(self,temp):
        self.SetPoint1(temp)
        time.sleep(1)
        self.Reset()
        time.sleep(1)
        self.SetPoint2(temp)
        time.sleep(1)
        self.Reset()

    def ReadSetPoint1(self):
        try:
            query="*R01\r"
            self.chiller.write(query.encode())
            value=self.chiller.readline()
            print(value)
            value=value.rstrip()
            temp=GetPoint(value)
            return temp
        except serial.SerialTimeoutException:
            print("Timeout: chiller not responding")

    def ReadSetPoint2(self):
        try:
            query="*R02\r"
            self.chiller.write(query.encode())
            value=self.chiller.readline()
            print(value)
            value=value.rstrip()
            temp=GetPoint(value)
            return int(temp)
        except serial.SerialTimeoutException:
            print("Timeout: chiller not responding")
            return None

    def GetTemperature(self):
        try:
            query="*X01\r"
            self.chiller.write(query.encode())
            temp=self.chiller.readline()
            temp=temp.rstrip()
            intemp = temp.lstrip('0')
            return float(intemp)
        except (ValueError,serial.SerialTimeoutException):
            print("Chiller not responding")
            return 25
    def Reset(self):
        query="*Z02\r"
        self.chiller.write(query.encode())
        
if __name__=="__main__":
    #chiller=RTE4()
    temp=float(sys.argv[1])
    chiller.SetTemperature(temp)
    chiller.close()
    
