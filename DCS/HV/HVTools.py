import matplotlib.pyplot as plt
import numpy as np
import time
import graphyte


def average_current(hv):
    current=0.0
    for i in range(5):
        current+=float(hv.GetCurrent())
        time.sleep(0.1)

    return current/5.0

#check if this should be +1000 or -1000
def IVCurve(hv,step=-5, end_limit=-500,begin_limit=0):
    hv.SetVoltageLevel(0)
    hv.ToRear()
    data_points=int(abs((int(end_limit)-int(begin_limit))/step))
    current_data=np.zeros(data_points)
    voltage_data=np.zeros(data_points)
    hv.SetVoltageRange(-100)
    graphyte.init("10.2.227.156")
    #ramp up
    for i in range(data_points):        
        voltage_data[i]=float(hv.GetVoltage())
        current_data[i]=average_current(hv)
        graphyte.send("IV_curve_current",curent_data[i])
        if(current_data[i]>=.0001):
            end_limit=i*step
            curent_data.resize(i,False)
            voltage_data.resize(i,False)
            break
        time.sleep(2)
        hv.SetVoltageLevel((i+1)*step)



    #ramp down
    voltage=end_limit
    while voltage <=0:
        hv.SetVoltageLevel(voltage)        
        voltage=voltage-5*step
        time.sleep(1)
    if(voltage !=0):
        hv.SetVoltageLevel(0)
    hv.TurnOff()
    ax=plt.subplot(111)
    plt.plot(np.absolute(voltage_data),np.absolute(current_data))
    plt.xlabel('Sensor Voltage')
    plt.ylabel('Sensor Current')
    plt.title('I-V')

    plt.savefig("IVCurve"+str(int(round(time.time())))+".pdf")

    return ax


