from __future__ import print_function
import time
import graphyte
import zmq
import threading
import thread
import re
import sht_sensor
import sys,os
try:
    import queue
except ImportError:
    import Queue as queue
if hasattr(__builtins__, 'raw_input'):
    input = raw_input
    
#local configuration

from initialize import SensorDict
from initialize import HighVoltage,LowVoltage,interlock,Chill,master,grafana
from HV import HVTools

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__
    
class ServerThread(threading.Thread):
    def __init__(self,threadID, CommQueue, HV=None, LV=None,Chiller=None,interlock=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.queue=CommQueue
        self.chiller = Chiller
        self.lock = threading.Lock()
        self.interlock=interlock
        self.HV = HV
        self.LV = LV

    def run(self):
        print("Starting " + str(self.threadID))
        self.server()

    def parse_chiller(self,message):
        try:
            temp = float(re.findall(r"-*\d+\.*\d*",message)[0])
        except IndexError:
            print("tried to set temperature but failed")
            return "failed to talk to chiller"
        with self.lock:
            print("setting temperature to",temp)
            try:
                self.chiller.SetTemperature(temp)
                return "set chiller temp to "+str(temp)
            except AttributeError:
                return "failed to talk to chiller"
            
            return "failed to talk to chiller"

    def parse_HV(self,message):
        data=re.findall(r"-*\d+\.*\d*",message)
        try:
            attr=float(data[0])
        except IndexError:
            attr=0.0
        try:
            channel=int(data[1])
        except IndexError:
            channel=1
            reply=""
        try:
            if 'on' in message:
                self.HV.TurnOn()
                reply="turned on"
            elif 'off' in message.lower():
                self.HV.TurnOff()
                reply= "turned off"
            else:
                with self.lock:
                    if('volt' in message.lower()):
                        if("protection" in message):
                            self.HV.SetVoltageCompliance(attr)
                            reply= "voltage protection set" +str(attr)
                        else:
                            self.HV.SetVoltageLevel(attr)
                            reply= "voltage  set" +str(attr)
                    else:
                        if("protection" in message):
                            self.HV.SetCurrentCompliance(attr)
                            reply= "current protection set" +str(attr)
                        else:
                            self.HV.SetCurrentLevel(attr,channel)
                            reply= "current set" +str(attr)
        except AttributeError:
            reply= "Failed to Talk To high voltage"
        return reply

    def parse_LV(self,message):
        data=re.findall(r"-*\d+\.*\d*",message)
        try:
            attr=float(data[0])
        except IndexError:
            attr=0.0
        try:
            channel=int(data[1])
        except IndexError:
            channel=1
        if 'on' in message.lower():
            self.LV.TurnOn()
            return "turned on"
        elif 'off' in message.lower():
            self.LV.TurnOff()
            return "turned off"
        else:
            with self.lock:
                if('volt' in message.lower()):
                    if("protection" in message):
                        self.LV.SetVProtection(channel,attr)
                        return "voltage protection set" +str(attr)
                    else:
                        self.LV.SetVoltage(channel,attr)
                        return "voltage set"+str(attr)
                else:
                    if("protection" in message):
                        self.LV.SetVProtection(channel,attr)
                        return "current protection set" +str(attr)
                    else:
                        self.LV.SetVoltage(channel,attr)
                        return "current set" +str(attr)

    def parse_interlock(self,config):
        with self.lock:
            self.interlock.set_SHT_temp(config['sht_temp_high'],config['sht_temp_low'])
            self.interlokc.set_humidity_range(config['sht_humidity_high'],config['sht_humidity_low'])
            self.interlock.set_user_NTC_temp(config['user_ntc_high'],config['user_ntc_low'])
            self.interlock.set_hybdrid_NTC_temp(config['hybrid_ntc_high'],config['hybdrid_ntc_low'])

    def start_sensors(self):
        sensor_thread=SensorDebugging("sensor thread",self.queue,Chiller=self.chiller,interlock=self.interlock)
        sensor_thread.start()

    def server(self):

        while True:
            print("Enter command (break to exit) :")
            blockPrint()
            message = input("Enter command (break to exit) : ")
            enablePrint()
            print ("recieved message: ",message)
            if "break" in message.lower():
                self.queue.put("break")
                break
                
            elif('iv curve' in message.lower()):
                thread.start_new_thread(HVTools.IVCurve, (self.HV,))
                continue
            elif "read" in message.lower():
                try:
                    channel=int(re.findall(r"\d+",message)[0])
                except IndexError:
                    channel=1

                if("high" in message.lower()):
                    HV_volt=self.HV.GetVoltage(channel)
                    HV_current=self.HV.GetCurrent(channel)
                    HV_resistance=self.HV.GetResistance(channel)
                    messsage = "HV: Voltage:"+str(HV_volt) +" Set Current:"+str(HV_current)+" Resistance: "+str(HV_resistance)
                elif('low' in message.lower()):
                    LV_set_volt=self.LV.ReadVoltage(channel)
                    LV_set_current=self.LV.ReadVoltage(channel)
                    LV_meas_volt=self.LV.GetActualVoltage(channel)
                    LV_meas_current=self.LV.GetActualCurrent(channel)
                    LV_OVP=self.LV.GetVoltageProtection(channel)
                    message =  "LV: Voltage:"+str(LV_set_volt) +" Current:"+str(LV_set_current)
                    message+=" Read Voltage: "+str(LV_meas_volt)+"\n"+" Read Current:" +str(LV_meas_current)+ " OVP :"+str(LV_OVP)                
                print(message)
                
            elif("chiller" in message.lower()):
                result=self.parse_chiller(message)
            elif("high" in message.lower() and "volt" in message.lower()):
                result=self.parse_HV(message)
            elif("low" in message.lower() and "volt" in message.lower()):
                result=self.parse_LV(message)
            elif('sensor' in message.lower()):
                print("starting sensors")
                if('on' in message.lower()):
                    self.queue.queue.clear()
                    self.start_sensors()
                else:
                    self.queue.put('break')
            else:
                pass

class SensorDebugging(threading.Thread):
    def __init__(self,threadID,comm_queue,Chiller=None,interlock=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chiller = Chiller
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.interlock=interlock
        global master

    def run(self):
        print ("Starting " + str(self.threadID))
        self.Read()

    def Read(self):
        command=""
        while True:
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
            with self.lock:
                try:
                    chiller_t = self.chiller.GetTemperature()
                    print("chiller temperature:"+str(chiller_t))
                    
                except AttributeError:
                    print("chiller not responding")

            with self.lock:
                try:
                    data=self.interlock.get_data()
                    data=data.split(",")
                    metrics=["hybrid NTC 1",
                             "hybrid NTC 2",
                             "hybrid NTC 3",
                             "hybrid NTC 4",
                             "hybrid NTC 5",
                             "hybrid NTC 6",
                             "hybrid NTC 7",
                             "hybrid NTC 8",
                             "user NTC 1",
                             "user NTC 2",
                             "SHT_temp",
                             "SHT_humidity"]
                    
                    for point,metric in zip(data,metrics):
                        point=int(point)
                        name="interlock."+metric
                        print(name+str(point))
                except AttributeError:
                    print("interlock not responding")
                    
            for label,sensor in SensorDict.items():
                print("label ",label)
                try:
                    for name,data in sensor.get_data().items():
                        grafana_name='OneModule.'+str(label)
                        grafana_name+="."+name
                        print(grafana_name,data)
                except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
                    print("SHT not responding")
                        
            time.sleep(5)


if __name__=="__main__":
	CommunicationQueue=queue.Queue()
	Thread1=ServerThread("Server Thread",CommunicationQueue,HV=HighVoltage,LV=LowVoltage,Chiller=Chill,interlock=interlock)
	Thread1.start()
